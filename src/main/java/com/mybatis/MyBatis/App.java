package com.mybatis.MyBatis;

import java.io.IOException;

import com.mybatis.MyBatis.entity.Message;
import com.mybatis.MyBatis.entity.User;
import com.mybatis.MyBatis.service.MessageService;
import com.mybatis.MyBatis.service.UserService;

public class App {
    public static void main(String[] args) throws IOException {
        
//        addMessage();
        readMessage();
    }

    static void addMessage() {
        System.out.println("START");

        Message message = new Message();
        message.setId(0);
        message.setTest("I'm first message");

        new MessageService().createMessage(message);
        System.out.println("END");

    }
    static void readMessage() {
        Message m = new MessageService().readMessage(0);
        System.out.println(m);
    }
    
    
    static void addUser() {
        System.out.println("START");

       User user = new User();
       user.setId(0);
       user.setName("My name");

        new UserService().createUser(user);
        System.out.println("END");

    }
}
