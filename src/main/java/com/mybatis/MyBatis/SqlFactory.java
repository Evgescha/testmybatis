package com.mybatis.MyBatis;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlFactory {
    private static SqlSessionFactory factory = null;
    static final String resource = "mybatis.cfg.xml";

    public static SqlSessionFactory getFactory() {
        if (factory == null) {
            InputStream inputStream;
            try {
                inputStream = Resources.getResourceAsStream(resource);
                factory = new SqlSessionFactoryBuilder().build(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return factory;

    }
}
