package com.mybatis.MyBatis.entity;

import lombok.Data;

@Data
public class Message {
    private String test;
    private int id;
}
