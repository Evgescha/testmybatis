package com.mybatis.MyBatis.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mybatis.MyBatis.entity.Message;

public interface MessageMapper {
    @Insert("INSERT INTO message(id,test) VALUES (#{id},#{test})")
    public void create(Message message);
    @Select("SELECT message.id, message.test FROM message WHERE id=#{id}")
    public Message read(Integer id);

    @Update("UPDATE message SET test='#{test}' WHERE id=#{id}")
    public void update(Message message);
    @Delete("DELETE FROM message WHERE id=#{id}")
    public void delete(Message message);
}
