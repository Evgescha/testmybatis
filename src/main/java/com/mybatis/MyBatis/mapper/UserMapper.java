package com.mybatis.MyBatis.mapper;

import com.mybatis.MyBatis.entity.User;

public interface UserMapper {
    public void create(User user);
}
