package com.mybatis.MyBatis.service;

import org.apache.ibatis.session.SqlSession;

import com.mybatis.MyBatis.SqlFactory;
import com.mybatis.MyBatis.entity.Message;
import com.mybatis.MyBatis.mapper.MessageMapper;

public class MessageService {

    public void createMessage(Message m) {
        SqlSession openSession = SqlFactory.getFactory().openSession();
        try {
            MessageMapper mm = openSession.getMapper(MessageMapper.class);
            mm.create(m);
            openSession.commit();
        } catch (Exception e) {
            
            e.printStackTrace();
        }finally {
            openSession.close();
        }

    };
     public Message readMessage(Integer id) {
         SqlSession openSession = SqlFactory.getFactory().openSession();
         Message m = null;
         try {
             MessageMapper mm = openSession.getMapper(MessageMapper.class);
             m = mm.read(id);
         } catch (Exception e) {
             
             e.printStackTrace();
         }finally {
             openSession.close();
         }
         return m;
     }
    // public void updateMessage(Message m) {}
    // public void deleteMessage(Message m) {}

}
