package com.mybatis.MyBatis.service;

import org.apache.ibatis.session.SqlSession;

import com.mybatis.MyBatis.SqlFactory;
import com.mybatis.MyBatis.entity.User;

public class UserService {

    public void createUser(User u) {
        SqlSession openSession = SqlFactory.getFactory().openSession();
        try {
            openSession.insert("com.mybatis.MyBatis.mapper.UserMapper.create", u);
            // UserMapper um = openSession.getMapper(UserMapper.class);
            // um.create(u);
            openSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            openSession.close();
        }

    };

}
